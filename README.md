# Winton Stock Market Challenge [ONGOING]
## Predicting stock returns

![Capa](https://gitlab.com/augusto.dp01/winton-stock-predictions/-/raw/master/img/capa.jpg)

Photo by [Lorenzo from Pexels](https://www.pexels.com/@lorenzocafaro).
# 1.0 - Business Problem

**This challenge was taken from a past Kaggle competition. You can access it [here](https://www.kaggle.com/c/the-winton-stock-market-challenge).**

Winton is a quantitative asset manager that uses statistical and mathematical inference to invest in global financial markets. One of the main jobs of these Financial Data Scientists is to discern the noise from the signal in the market. Here, the job comes to us. We are given a few features and some daily and intraday returns, and it is asked of us the prediction for the intraday returns for the rest of the present day and daily for the next two days.

# 2.0 - Business Assumptions

**01:** The intraday returns given cover the whole day: Ret_2 refers to the first return of the day, and Ret_180 refers to the last.

**02:** The behavior of an intraday return is similar to it's adjacents.

# 3.0 - Solution Strategy

The solution will be given through 10 steps. These are:

**01. Descriptive Data Analysis:** First we will use descriptive statistics techniques. The goal of this step is not to judge if a feature is relevant to the prediction, but rather to get familiarity with the data.

**02. Feature Engineering:** Then we will derive features from the ones that we have. Since we don't know with what features we are dealing, there is not much we can do in this step.

**03. Variable Filtering:** Here we will drop columns and lines that are not useful for our analysis or are not available for prediction.

**04. Exploratory Data Analysis:** The main goal here is to know how each feature affects the response variable and how they relate to each other.

**05. Data Preparation:** This step is geared towards the optimization of the machine learning process. We transform the data as to get it standardized and rescaled.

**06. Feature Selection:** We use an algorithm to determine which features are useful for prediction for dimentionality reductio purposes. After that we validate the algorithm's choices to see if it agrees with our exploratory data analysis. This step is crucial, since one of the main goals is to know what features are not noise, so we will stress this step a bit more than usual.

**07. Machine Learning Modeling:** Here we train and cross validate models using the Occam's Razor criterion, from the less complexity models to the more sophisticated ones. Then we choose which model we will take to the deploy.

**08. Hyperparameter Fine Tunning:** In this step we run a search method to decide the best hyperparameters for our model.

**09. Interpreting the Error:** We take the measured error and translate them in our predictions, from our regular measures (MAE, RMSE) to more business suited units. In this case, we will run a cross validated backtest, which is more suited for trading purposes.

**10. Model Deploy:** Finally we write the code for every step of our pipeline and deploy it on the internet so people can access our results.

# 4.0 - Data Insights

## 4.1 - Volatility raises along the day.

The first intraday returns of the day are more concentrated around 0, with a very direct correlation between time of day and standard deviation.

Here is a plot of the standard deviation of returns relative to the time of day.

![Returns Std vs Time of Day](https://gitlab.com/augusto.dp01/winton-stock-predictions/-/raw/master/img/scatter_std.png)

And a plot of the quartile values from the returns distribution relative to the time of day.

![IQ Values of returns vs Time of Day](https://gitlab.com/augusto.dp01/winton-stock-predictions/-/raw/master/img/scatter_iq.png)

## 4.2 - Almost half of the return is built on the overnight.

When we tried to derive the return from the present day from the intraday returns, we got a feature with similar behavior, but with very smaller range and interquartile value.

![Returns Distribution](https://gitlab.com/augusto.dp01/winton-stock-predictions/-/raw/master/img/ret_zero.png)

Orange, Ret_MinusOne returns; blue, Ret_Zero

## 4.3 - Little directional information can be gathered from previous returns, but we can have some info on the volatility.

In none of the many plots we made throughout the analysis was possible to see a discernible directional trend to any of the features or returns. But in many there were informations like is seen below. We can see that for low values of the feature, both the range and quantile values are further from naught.

![Feature_13 vs Ret_PlusOne](https://gitlab.com/augusto.dp01/winton-stock-predictions/-/raw/master/img/feat13_returns.png)

# 5.0 - Relevant Features

In our first run of the Boruta algorithm, using Random Forest as our test model, some of the features were deemed irrelevant, but none of the intraday returns were so, not even for predictions of two days away returns. Since this is rather unexpected, we shall try running the selection again, but this time with another ML Algorithm.

# 6.0 - Machine Learning Models

# 7.0 - Machine Learning Performance

# 8.0 - Business Results

# 9.0 - Conclusions

# 10.0 - Next Steps to Improve
